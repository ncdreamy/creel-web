<?php
	require_once('connect.class.php');
	class fishcaught
	{
		// Get all fish caught
		public function getAllFishCaught(){
			$dbcon= new connect();
			$qry=$dbcon->db1->prepare("SELECT * FROM tblfishcaught");
			$qry->execute();
			$res = $qry->fetchAll(); // Fetches record
			return $res;
		}

		// Get entire row using fish caught UID
		public function getFishByFishCaughtUID($fishCaughtUID){
			$dbcon = new connect();
			$qryString = "SELECT fish.*, spec.speciesCommonName 
							FROM tblfishcaught fish, tblspecies spec 
							WHERE fishCaughtUID=:fishCaughtUID
							AND fish.speciesCode = spec.speciesCode";
			$qry=$dbcon->db1->prepare($qryString);
			$qry->bindParam(":fishCaughtUID",$fishCaughtUID,PDO::PARAM_STR);
			$qry->execute();
			$res = $qry->fetch();
			return $res;
		}

		// Get fish record by angler UID
		public function getFishByAnglerUID($anglerUID){
			$dbcon = new connect();
			$qryString = "SELECT fish.*, spec.speciesCommonName
							from tblfishcaught fish, tblspecies spec
							WHERE anglerUID = :anglerUID
							AND fish.speciesCode = spec.speciesCode";
			$qry=$dbcon->db1->prepare($qryString);
			$qry->bindParam(":anglerUID",$anglerUID,PDO::PARAM_STR);
			$qry->execute();
			$res = $qry->fetchAll();
			return $res;
		}

		// Get fish image by fish caught UID
		public function getFishImageByFishCaughtUID($fishCaughtUID){
			$dbcon = new connect();
			$qryString = "SELECT * from tblfishcaughtimages WHERE fishCaughtUID=:fishCaughtUID";
			$qry=$dbcon->db1->prepare($qryString);
			$qry->bindParam(":fishCaughtUID",$fishCaughtUID,PDO::PARAM_STR);
			$qry->execute();
			$res = $qry->fetch();
			return $res;
		}
	}
?>