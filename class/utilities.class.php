<?php
	include_once("projectUser.class.php");

	class utilities{

		// Convert base64 from MySQL to JPG
		public function base64_to_jpeg($base64_string) {
		    $src = 'data:image/jpg;base64,'.$base64_string;
		    return $src;
		}

		// Convert base64 from MySQL to PNG
		public function base64_to_png($base64_string) {
		    $src = 'data:image/png;base64,'.$base64_string;
		    return $src;
		}

		// Get Trip label
		public function getDestination($destination){
			$destinationLabel = "";
			switch($destination){
				case "0":
					$destinationLabel = "No";
					break;
				case "1":
					$destinationLabel = "Yes";
					break;
				case "NA":
					$destinationLabel = "Refused";
					break;
				default:
					$destinationLabel = "Unknown";
					break;
			}			
			return $destinationLabel;
		}

		// Get Trip label
		public function getTrip($trip){
			$tripLabel = "";
			switch($trip){
				case "C":
					$tripLabel = "Completed";
					break;
				case "I":
					$tripLabel = "Incomplete";
					break;
				case "NA":
					$tripLabel = "Refused";
					break;
				default:
					$tripLabel = "Unknown";
					break;
			}			
			return $tripLabel;
		}

		// Get Race Label
		public function getRace($race){
			$raceLabel = "";
			switch($race){
				case "W":
					$raceLabel = "White";
					break;
				case "B":
					$raceLabel = "Black";
					break;
				case "NA":
					$raceLabel = "Refused";
					break;
				default:
					$raceLabel = "Other";
					break;
			}
			return $raceLabel;
		}


		// Get Fish Type 
		public function getFishingType($fType){
			$fishingType = "";
			switch($fType){
				case "1":
					$fishingType = "Boat";
					break;
				case "2":
					$fishingType = "Bank/Dock";
					break;
				case "3":
					$fishingType = "Heat Dock";
					break;
				case "4":
					$fishingType = "Canoe";
					break;
				case "5":
					$fishingType = "Wade/Belly";
					break;
				case "6":
					$fishingType = "Handicap";
					break;
				case "NA":
					$fishingType = "Refused";
					break;
				default:
					$fishingType = "No";
					break;
			}
			return $fishingType;
		}

		// Get Lure 
		public function getLure($lure){
			$lureLabel = "";
			switch($lure){
				case "1":
					$lureLabel = "Art";
					break;
				case "2":
					$lureLabel = "Fly";
					break;
				case "3":
					$lureLabel = "Nat";
					break;
				case "4":
					$lureLabel = "Prepared";
					break;
				case "5":
					$lureLabel = "Combo";
					break;
				case "NA":
					$lureLabel = "Refused";
					break;
				default:
					$lureLabel = "Unknown";
					break;
			}
			return $lureLabel;
		}

		// Get Method 
		public function getMethod($method){
			$methodLabel = "";
			switch($method){
				case "1":
					$methodLabel = "Still";
					break;
				case "2":
					$methodLabel = "Cast";
					break;
				case "3":
					$methodLabel = "Troll";
					break;
				case "4":
					$methodLabel = "Drift";
					break;
				case "5":
					$methodLabel = "Set/Radi";
					break;
				case "6":
					$methodLabel = "Gig";
					break;
				case "7":
					$methodLabel = "Trotline";
					break;
				case "8":
					$methodLabel = "Jug";
					break;
				case "9":
					$methodLabel = "Snag";
					break;
				case "0":
					$methodLabel = "Bow";
					break;
				case "NA":
					$methodLabel = "Refused";
					break;
				default:
					$methodLabel = "Unknown";
					break;
			}
			return $methodLabel;
		}


		/** Fishing Panel **/
		// Get Measured Label
		public function getMeasured($measured){
			$measuredLabel = "";
			switch($measured){
				case "Y":
					$measuredLabel = "Yes, By MDC";
					break;
				case "N":
					$measuredLabel = "Yes, but NOT by MDC";
					break;
				case "NZ":
					$measuredLabel = "No";
					break;
				case "NA":
					$measuredLabel = "Refused";
					break;
				default:
					$measuredLabel = "No";
					break;
			}
			return $measuredLabel;
		}

		// Get Random UID
		public function randomNumber(){
		    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		        // 32 bits for "time_low"
		        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

		        // 16 bits for "time_mid"
		        mt_rand( 0, 0xffff ),

		        // 16 bits for "time_hi_and_version",
		        // four most significant bits holds version number 4
		        mt_rand( 0, 0x0fff ) | 0x4000,

		        // 16 bits, 8 bits for "clk_seq_hi_res",
		        // 8 bits for "clk_seq_low",
		        // two most significant bits holds zero and one for variant DCE1.1
		        mt_rand( 0, 0x3fff ) | 0x8000,

		        // 48 bits for "node"
		        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		    );
		}

		// Get Table data to download for CSV
		public function getTableData($tableName, $userUID){
			$dbcon= new connect();
		    $queryString = "SELECT * FROM ".$tableName;
		    $whereClause = "";
		    $projectUser = new projectUser();
		    $projectList = $projectUser->getProjectListByUser($userUID);

		    if(count($projectList) == 0){
		    	echo json_encode(0);
		    	return;
		    }

		    if(strcasecmp($tableName,"tblspecies") != 0){
		    	$whereClause = $whereClause." WHERE projectUID IN (";
		    	for($i=0; $i<count($projectList)-1;$i++){
		    		$projectUID = $projectList[$i]["projectUID"];
		    		$whereClause = $whereClause."'".$projectUID."', ";
		    	}
		    	$whereClause = $whereClause."'".$projectList[count($projectList)-1]["projectUID"]."')";
		   	}	

		   	$queryString = $queryString.$whereClause;

		   	// EXECUTE QUERY
		    $qry=$dbcon->db1->prepare($queryString);
		    $qry->execute();
		    $res = $qry->fetchAll(PDO::FETCH_ASSOC); // Fetches record
		    echo json_encode($res);
		    return;
		}
	}
?>