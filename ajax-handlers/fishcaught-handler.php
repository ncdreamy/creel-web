<?php
	include_once('../class/fishcaught.class.php');
		
	// Get Fish Details and Image (if exists) 
	// from tblFishCaught and tblFishCaughtImages table
	if(isset($_POST["fishCaughtUID"])){
		$tblFishCaught = new fishCaught();
		$fishCaughtUID = $_POST["fishCaughtUID"];
		
		$fishCaught = $tblFishCaught->getFishByFishCaughtUID($fishCaughtUID);
		$fishCaughtImage = "";
		if($fishCaught["photoTaken"] == "1"){
			$fishCaughtImage = $tblFishCaught->getFishImageByFishCaughtUID($fishCaughtUID);
		}

		$arr = [];
		$arr["fishCaught"] = $fishCaught;
		$arr["fishCaughtImageDetails"] = $fishCaughtImage;
		
		echo json_encode($arr);
	}
?>